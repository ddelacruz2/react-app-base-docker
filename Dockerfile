FROM node:16
WORKDIR /app
COPY package*.json ./
RUN npm install
COPY . .
# ENV REACT_APP_NAME=test
EXPOSE 5173
CMD ["yarn", "dev"]