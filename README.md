# Proyeto base react

## Levantar el proyecto
**Requiere tener docker instalado**

- Ubicarse en la raiz del proyecto y ejecutar el siguiente comando
  ```bash
    docker compose up --build
    ```

- El proyecto corre en el puerto 5173
    [http://localhost:5173/](http://localhost:5173/) 

- Como instalar nuevas dependencias
  ```bash
    docker compose exec app yarn add nombre-de-la-libreria 
    ```

- Comando linter
  ```bash
    docker compose exec app yarn lint 
    ```

- Detener el contenerdor
  ```bash
    docker compose down 
    ```